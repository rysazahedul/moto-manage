from common.json import ModelEncoder, DateEncoder
from .models import Technician, Appointment


class TechnicianEncoder(ModelEncoder):
    model = Technician
    properties = [
        "first_name",
        "last_name",
        "employee_id",
        "id",
    ]


class TechnicianAppointmentEncoder(ModelEncoder):
    model = Technician
    properties = [
        "first_name",
        "last_name",
    ]


class AppointmentEncoder(ModelEncoder):
    model = Appointment
    properties = [
        "date_time",
        "reason",
        "status",
        "vin",
        "customer",
        "VIP",
        "technician",
        "id",
    ]
    encoders = {
        "technician": TechnicianAppointmentEncoder(),
        "datetime": DateEncoder(),
    }

    def get_extra_data(self, o):
        return {"status": o.status.name}
