import React, { useState, useEffect } from "react";

function SalespersonList() {
  const [salespeople, setSalespeople] = useState([]);

  const fetchData = async () => {
    const salespeopleResponse = await fetch(
      "http://localhost:8090/api/salespeople"
    );
    if (salespeopleResponse.ok) {
      const salespeopleData = await salespeopleResponse.json();
      setSalespeople(salespeopleData.salespeople);
    }
  };
  useEffect(() => {
    fetchData();
  }, []);

  return (
    <>
      <h1>Salespeople</h1>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Employee ID</th>
            <th>First Name</th>
            <th>Last Name</th>
          </tr>
        </thead>
        <tbody>
          {salespeople?.map((salesperson) => {
            return (
              <tr key={salesperson.id}>
                <td>{salesperson.employee_id}</td>
                <td>{salesperson.first_name}</td>
                <td>{salesperson.last_name}</td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </>
  );
}

export default SalespersonList;
