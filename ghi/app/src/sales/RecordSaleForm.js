import React, { useEffect, useState } from "react";

function RecordSaleForm() {
  const [automobiles, setAutomobiles] = useState([]);
  const [automobile, setAutomobile] = useState("");
  const [salespeople, setSalespeople] = useState([]);
  const [salesperson, setSalesperson] = useState("");
  const [customers, setCustomers] = useState([]);
  const [customer, setCustomer] = useState("");
  const [price, setPrice] = useState(0);

  const fetchAutomobiles = async () => {
    const url = "http://localhost:8100/api/automobiles/";
    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      setAutomobiles(data.autos);
    }
  };

  useEffect(() => {
    fetchAutomobiles();
  }, []);

  const fetchSalespeople = async () => {
    const url = "http://localhost:8090/api/salespeople/";
    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      setSalespeople(data.salespeople);
    }
  };

  useEffect(() => {
    fetchSalespeople();
  }, []);

  const fetchCustomers = async () => {
    const url = "http://localhost:8090/api/customers/";
    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      setCustomers(data.customers);
    }
  };

  useEffect(() => {
    fetchCustomers();
  }, []);

  const carSold = async () => {
    const fetchOptions = {
      method: "PUT",
      body: JSON.stringify({ "sold": true }),
    };
    const response = await fetch(
      `http://localhost:8100/api/automobiles/${automobile}/`,
      fetchOptions
    );
    if (response.ok) {
      const data = await response.json();
    }
  };

  const handleSubmit = async (event) => {
    event.preventDefault();
    const data = {};

    data.automobile = automobile;
    data.sold = true;
    data.salesperson = salesperson;
    data.customer = customer;
    data.price = price;

    const salesUrl = "http://localhost:8090/api/sales/";
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
      },
    };
    const response = await fetch(salesUrl, fetchConfig);
    if (response.ok) {
      const newSale = await response.json();
      await carSold();
      setAutomobile("");
      setSalesperson("");
      setCustomer("");
      setPrice("");
    }
  };

  const handleAutomobileChange = (event) => {
    const value = event.target.value;
    setAutomobile(value);
  };

  const handleSalespersonChange = (event) => {
    const value = event.target.value;
    setSalesperson(value);
  };

  const handleCustomerChange = (event) => {
    const value = event.target.value;
    setCustomer(value);
  };

  const handlePriceChange = (event) => {
    const value = event.target.value;
    setPrice(parseInt(value));
  };

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Record a New Sale</h1>
          <form onSubmit={handleSubmit} id="create-sale-form">
            <div className="mb-3">
              <select
                value={automobile}
                onChange={handleAutomobileChange}
                required
                name="automobile"
                id="automobile"
                className="form-select"
              >
                <option value="">Choose an automobile</option>
                {automobiles?.map(automobile => {
                  return (
                    <option key={automobile.vin} value={automobile.vin}>
                      {automobile.vin}
                    </option>
                  );
                })}
              </select>
            </div>
            <div className="mb-3">
              <select
                value={salesperson}
                onChange={handleSalespersonChange}
                required
                name="salesperson"
                id="salesperson"
                className="form-select"
              >
                <option value="">Choose a Salesperson</option>
                {salespeople.map((salesperson) => {
                  return (
                    <option
                      key={salesperson.employee_id}
                      value={salesperson.employee_id}
                    >
                      {salesperson.first_name} {salesperson.last_name}
                    </option>
                  );
                })}
              </select>
            </div>
            <div className="mb-3">
              <select
                value={customer}
                onChange={handleCustomerChange}
                required
                name="customer"
                id="customer"
                className="form-select"
              >
                <option value="">Choose a Customer</option>
                {customers.map((customer) => {
                  return (
                    <option key={customer.id} value={customer.id}>
                      {customer.first_name} {customer.last_name}
                    </option>
                  );
                })}
              </select>
            </div>
            <div className="form-floating mb-3">
              <input
                value={price}
                onChange={handlePriceChange}
                placeholder="Price"
                required
                type="text"
                name="price"
                id="price"
                className="form-control"
              />
              <label htmlFor="price">Price</label>
            </div>
            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
  );
}

export default RecordSaleForm;
