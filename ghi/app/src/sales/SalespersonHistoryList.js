import React, { useState, useEffect } from "react";

function SalespersonHistoryList() {
  const [salespeople, setSalespeople] = useState([]);
  const [sales, setSales] = useState([]);
  const [selectedSalesperson, setSelectedSalesperson] = useState("");

  const fetchSalespeople = async () => {
    const salespeopleResponse = await fetch(
      "http://localhost:8090/api/salespeople/"
    );
    if (salespeopleResponse.ok) {
      const salespeopleData = await salespeopleResponse.json();
      setSalespeople(salespeopleData.salespeople);
    }
  };

  const fetchSales = async () => {
    const salesResponse = await fetch("http://localhost:8090/api/sales/");
    if (salesResponse.ok) {
      const salesData = await salesResponse.json();
      setSales(salesData.sales);
    }
  };

  const handleEmployeeIdChange = async (event) => {
    const value = event.target.value;
    setSelectedSalesperson(value);

    if (value === "") {
      setSales([]);
    } else {
      const filteredSalesperson = sales.filter(
        (sale) => sale.salesperson.employee_id == value
      );
      setSales(filteredSalesperson);
    }
  };

  useEffect(() => {
    fetchSalespeople();
    fetchSales();
  }, []);

  return (
    <>
      <h1>Salesperson History</h1>
      <div className="mb-3">
        <select
          value={selectedSalesperson}
          onChange={handleEmployeeIdChange}
          required
          id="employeeId"
          name="employeeId"
          className="form-select"
        >
          <option value="">Choose a Salesperson</option>
          {salespeople.map((salesperson) => {
            return (
              <option
                key={salesperson.employee_id}
                value={salesperson.employee_id}
              >
                {salesperson.first_name} {salesperson.last_name}
              </option>
            );
          })}
        </select>
      </div>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Salesperson</th>
            <th>Customer</th>
            <th>VIN</th>
            <th>Price</th>
          </tr>
        </thead>
        <tbody>
          {sales.map((sale) => {
            return (
              <tr key={sale.id}>
                <td>
                  {sale.salesperson.first_name} {sale.salesperson.last_name}
                </td>
                <td>
                  {sale.customer.first_name} {sale.customer.last_name}
                </td>
                <td>{sale.automobile.vin}</td>
                <td>${sale.price}</td>
              </tr>
            );
          })}
        </tbody>
      </table>
      <button onClick={fetchSales}>See all</button>
    </>
  );
}

export default SalespersonHistoryList;
