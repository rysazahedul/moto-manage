import React, { useState, useEffect } from "react";

function ModelList() {
  const [models, setModels] = useState([]);

  const fetchData = async () => {
    const modelResponse = await fetch("http://localhost:8100/api/models/");
    if (modelResponse.ok) {
      const modelData = await modelResponse.json();
      setModels(modelData.models);
    }
  };

  useEffect(() => {
    fetchData();
  }, []);

  return (
    <div>
      <div>
        <div>
          <h1>Models</h1>
          <table className="table table-striped">
            <thead>
              <tr>
                <th>Name</th>
                <th>Manufacturer</th>
                <th>Picture</th>
              </tr>
            </thead>
            <tbody>
              {models.map((model) => {
                return (
                  <tr key={model.id}>
                    <td>{model.name}</td>
                    <td>{model.manufacturer.name}</td>
                    <td>
                      <img
                        src={model.picture_url}
                        alt="car"
                        width="250"
                        height="140"
                      />
                    </td>
                  </tr>
                );
              })}
            </tbody>
          </table>
        </div>
      </div>
    </div>
  );
}

export default ModelList;
