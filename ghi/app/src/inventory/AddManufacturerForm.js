import React, { useState } from "react";

function AddManufacturerForm() {
  const [formData, setFormData] = useState({
    name: "",
  });

  const handleFormChange = (event) => {
    const inputName = event.target.name;
    const value = event.target.value;
    setFormData({
      ...formData,
      [inputName]: value,
    });
  };

  const handleSubmit = async (event) => {
    event.preventDefault();
    const manufacturerUrl = "http://localhost:8100/api/manufacturers/";
    const fetchConfig = {
      method: "POST",
      body: JSON.stringify(formData),
      headers: {
        "Content-Type": "application/json",
      },
    };

    const response = await fetch(manufacturerUrl, fetchConfig);
    if (response.ok) {
      setFormData({
        name: "",
      });
    }
  };

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Add a Manufacturer</h1>
          <form onSubmit={handleSubmit} id="create-technician-form">
            <div className="form-floating mb-3">
              <input
                onChange={handleFormChange}
                value={formData.name}
                placeholder="employee_id"
                required
                type="text"
                name="name"
                id="name"
                className="form-control"
              />
              <label htmlFor="name">Name</label>
            </div>

            <button className="btn btn-primary">Add Manufacturer</button>
          </form>
        </div>
      </div>
    </div>
  );
}

export default AddManufacturerForm;
