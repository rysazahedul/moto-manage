import { BrowserRouter, Routes, Route } from "react-router-dom";
import MainPage from "./MainPage";
import Nav from "./Nav";
import TechnicianList from "./services/TechnicianList";
import AddTechnicianForm from "./services/AddTechnicianForm";
import AddAppointmentForm from "./services/AddAppointmentForm";
import AppointmentList from "./services/AppointmentList";
import ServiceHistory from "./services/ServiceHistory";
import ManufacturerList from "./inventory/ManufacturersList";
import AddManufacturerForm from "./inventory/AddManufacturerForm";
import ModelList from "./inventory/ModelList";
import SalespersonForm from "./sales/SalespersonForm";
import CustomerForm from "./sales/CustomerForm";
import SalespersonList from "./sales/SalespersonList";
import CustomerList from "./sales/CustomerList";
import SalesList from "./sales/SalesList";
import RecordSaleForm from "./sales/RecordSaleForm";
import SalespersonHistoryList from "./sales/SalespersonHistoryList";
import ModelForm from "./inventory/ModelForm";
import AutomobileForm from "./inventory/AutomobileForm";
import AutomobileList from "./inventory/AutomobileList";

function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="technicians">
            <Route path="" element={<TechnicianList />} />
            <Route path="add" element={<AddTechnicianForm />} />
          </Route>
          <Route path="appointments">
            <Route path="" element={<AppointmentList />} />
            <Route path="add" element={<AddAppointmentForm />} />
            <Route path="history" element={<ServiceHistory />} />
          </Route>
          <Route path="manufacturers">
            <Route path="" element={<ManufacturerList />} />
            <Route path="add" element={<AddManufacturerForm />}></Route>
          </Route>
          <Route path="models">
            <Route path="" element={<ModelList />} />
            <Route path="add" element={<ModelForm />} />
          </Route>
          <Route path="sales" element={<SalesList />} />
          <Route path="customers" element={<CustomerList />} />
          <Route path="salespeople" element={<SalespersonList />} />
          <Route path="saleshistory" element={<SalespersonHistoryList />} />
          <Route path="salespeople">
            <Route path="add" element={<SalespersonForm />} />
          </Route>
          <Route path="customers">
            <Route path="add" element={<CustomerForm />} />
          </Route>
          <Route path="sales">
            <Route path="add" element={<RecordSaleForm />} />
          </Route>
          <Route path="automobiles">
            <Route path="add" element={<AutomobileForm />} />
          </Route>
          <Route path="automobiles">
            <Route path="" element={<AutomobileList />} />
          </Route>
          <Route path="automobiles">
            <Route path="" element={<AutomobileList />} />
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
