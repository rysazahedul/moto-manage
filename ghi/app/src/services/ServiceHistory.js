import React, { useState, useEffect } from "react";

function ServiceHistory() {
  const [appointments, setAppointments] = useState([]);
  const [vin, setVin] = useState("");
  const [filterAppointment, setFilterAppointment] = useState([]);

  const fetchData = async () => {
    const appointmentResponse = await fetch(
      "http://localhost:8080/api/appointments/"
    );
    if (appointmentResponse.ok) {
      const appointmentData = await appointmentResponse.json();
      setAppointments(appointmentData.appointments);
      setFilterAppointment(appointmentData.appointments);
    }
  };

  useEffect(() => {
    fetchData();
  }, []);

  const handleSearchChange = (event) => {
    event.preventDefault();
    if (vin) {
      setFilterAppointment(
        appointments.filter((appointment) => appointment.vin.includes(vin))
      );
    }
  };

  const handleInputChange = (event) => {
    setVin(event.target.value);
  };

  return (
    <div>
      <div>
        <div>
          <h1>Service Appointments</h1>
          <form onSubmit={handleSearchChange} id="filter-appointment-by-vin">
            <div className="input-group-append">
              <input
                type="text"
                placeholder="Search for vin"
                onChange={handleInputChange}
                value={vin}
              />
              <button>Search</button>
            </div>
          </form>
          <table className="table table-striped">
            <thead>
              <tr>
                <th>Vin</th>
                <th>Is VIP?</th>
                <th>Customer</th>
                <th>Date</th>
                <th>Time</th>
                <th>Technician</th>
                <th>Reason</th>
                <th>Status</th>
              </tr>
            </thead>
            <tbody>
              {filterAppointment.map((appointment) => {
                if (appointment.status !== "CREATED") {
                  return (
                    <tr key={appointment.id}>
                      <td>{appointment.vin}</td>
                      <td>{appointment.VIP}</td>
                      <td>{appointment.customer}</td>
                      <td>
                        {new Date(appointment.date_time).toLocaleDateString()}
                      </td>
                      <td>
                        {new Date(appointment.date_time).toLocaleTimeString()}
                      </td>
                      <td>
                        {appointment.technician.first_name}{" "}
                        {appointment.technician.last_name}
                      </td>
                      <td>{appointment.reason}</td>
                      <td>{appointment.status}</td>
                    </tr>
                  );
                }
              })}
            </tbody>
          </table>
        </div>
      </div>
    </div>
  );
}

export default ServiceHistory;
