import React, { useState, useEffect } from "react";

function AppointmentList() {
  const [appointments, setAppointments] = useState([]);

  const fetchData = async () => {
    const appointmentResponse = await fetch(
      "http://localhost:8080/api/appointments/"
    );
    if (appointmentResponse.ok) {
      const appointmentData = await appointmentResponse.json();
      setAppointments(appointmentData.appointments);
    }
  };

  useEffect(() => {
    fetchData();
  }, []);

  const handleCancelAppointment = async (id) => {
    const response = await fetch(
      `http://localhost:8080/api/appointments/${id}/cancel`,
      { method: "PUT" }
    );
    if (response.ok) {
      fetchData();
    }
  };

  const handleFinishAppointment = async (id) => {
    const response = await fetch(
      `http://localhost:8080/api/appointments/${id}/finish`,
      { method: "PUT" }
    );
    if (response.ok) {
      fetchData();
    }
  };

  return (
    <div>
      <div>
        <div>
          <h1>Service Appointments</h1>
          <table className="table table-striped">
            <thead>
              <tr>
                <th>Vin</th>
                <th>Is VIP?</th>
                <th>Customer</th>
                <th>Date</th>
                <th>Time</th>
                <th>Technician</th>
                <th>Reason</th>
              </tr>
            </thead>
            <tbody>
              {appointments.map((appointment) => {
                if (appointment.status === "CREATED") {
                  return (
                    <tr key={appointment.id}>
                      <td>{appointment.vin}</td>
                      <td>{appointment.VIP}</td>
                      <td>{appointment.customer}</td>
                      <td>
                        {new Date(appointment.date_time).toLocaleDateString()}
                      </td>
                      <td>
                        {new Date(appointment.date_time).toLocaleTimeString()}
                      </td>
                      <td>
                        {appointment.technician.first_name}{" "}
                        {appointment.technician.last_name}
                      </td>
                      <td>{appointment.reason}</td>
                      <td>
                        <button
                          className="btn btn-danger"
                          onClick={() =>
                            handleCancelAppointment(appointment.id)
                          }
                        >
                          Cancel
                        </button>
                      </td>
                      <td>
                        <button
                          className="btn btn-success"
                          onClick={() =>
                            handleFinishAppointment(appointment.id)
                          }
                        >
                          Finish
                        </button>
                      </td>
                    </tr>
                  );
                }
              })}
            </tbody>
          </table>
        </div>
      </div>
    </div>
  );
}

export default AppointmentList;
