from django.urls import path
from .views import list_salespeople, salesperson_detail, list_customers, customer_detail, list_sales, sale_detail

urlpatterns = [
    path("salespeople/", list_salespeople, name="list_salespeople"),
    path("salespeople/<int:id>/", salesperson_detail, name="delete_salesperson"),
    path("customers/", list_customers, name="list_customers"),
    path("customers/<int:id>/", customer_detail, name="delete_customer"),
    path("sales/", list_sales, name="list_sales"),
    path("sales/<int:id>/", sale_detail, name="delete_sale"),
]
